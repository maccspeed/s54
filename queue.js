let collection = [];

// Write the queue functions below.
function print() {
    return collection;
}

function enqueue(val) {
    collection[collection.length] = val;
    return collection;
}

function dequeue() {
    let newArray = [];
    // for (i = 0; i < collection.length-1; i++) {
    //     newArray[newArray.length] = collection[i];
    // }
    for (i = collection.length-1; i > 0; i--) {
        newArray[newArray.length] = collection[i];
    }
    return collection = newArray;
}

function front() {
    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
    if (collection.length == 0 || collection == null) {
        return true;
    } else {
        return false;
    }
}

module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};



// [1] Print queue elements.
//     × The printed value is an empty array. RESULT = [] (4 ms)
//   [2] Enqueue a new element.
//     × The value has been enqueued. RESULT = ['John'] (1 ms)
//   [3] Enqueue another element.
//     × The value has been enqueued. RESULT = ['John', 'Jane'] (1 ms)
//   [4] Dequeue the first element.
//     × The value has been dequeued. RESULT = ['Jane'] (1 ms)
//   [5] Enqueue another element.
//     × The value has been enqueued. RESULT = ['Jane', 'Bob'] (1 ms)
//   [6] Enqueue another element.
//     × The value has been enqueued. RESULT = ['Jane', 'Bob', 'Cherry']
//   [7] Get first element.
//     × The first value has been retrieved. RESULT = 'Jane' (1 ms)
//   [8] Get queue size.
//     × The size of the queue has been retrieved. RESULT = 3 (1 ms)
//   [8] Check if queue is not empty.
//     × The result has been retrieved. RESULT = false